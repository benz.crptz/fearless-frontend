import { NavLink } from "react-router-dom";

function Nav() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">Conference Go!</NavLink>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/locations/new">New location</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/conferences/new">New conference</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="presentations/new">New presentation</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/attendees/new">New attendee</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/attendees">Attendees</NavLink>
              </li>


            </ul>
          </div>
        </div>
      </nav>
    );
  }

  export default Nav;

