import React, {useEffect, useState} from 'react';


function PresentationForm() {
    const [presenterName, setPresenterName] = useState('')
    const [presenterEmail, setPresenterEmail] = useState('')
    const [companyName, setCompanyName] = useState('')
    const [title, setTitle] = useState('')
    const [synopsis, setSynopsis] = useState('')
    const [conferences, setConferences] = useState([])
    const [conference, setConference] = useState('');
    console.log(conferences)

    const getData = async () =>{
        const response = await fetch("http://localhost:8000/api/conferences/");

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setConferences(data.conferences);
        } else{
        console.error("An error occurred fetching the data")
        }
    }

    useEffect(()=> {
        getData()}, []);

    function handlePresenterNameChange(event) {
        const {value} = event.target;
        setPresenterName(value);
    }

    function handlePresenterEmaileChange(event) {
        const {value} = event.target;
        setPresenterEmail(value);
    }

    function handleCompanyChange(event) {
        const {value} = event.target;
        setCompanyName(value);
    }

    function handleTitleChange(event) {
        const {value} = event.target;
        setTitle(value);
    }

    function handleSynopsisChange(event) {
        const {value} = event.target;
        setSynopsis(value);
    }

    function handleConferenceChange(event) {
        const {value} = event.target;
        setConference(value);
    }

    async function handleSubmit(event) {
        event.preventDefault()

        const data = {
            presenter_name : presenterName,
            presenter_email : presenterEmail,
            company_name : companyName,
            title,
            synopsis,
        };
        console.log(conference)
        const conferenceId = data.conferences;

        const locationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "content-Type" : "application/json",
            },
        };

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json();

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} value={presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label for="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmaileChange} value={presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label for="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label for="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label for="title">Title</label>
              </div>
              <div className="mb-3">
                <label for="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" class="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} erequired name="conference" id="conference" className="form-select">
                  <option selected value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
}
export default PresentationForm;
